/* *****************************************************************************
 * Project name: List
 * File name   : list
 * Author      : Damien Nguyen
 * Date        : Friday, March 06 2020
 * ****************************************************************************/

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "list.h"

bool l_is_empty(list_t list) {
    return !list;
}

int l_index_of(list_t list, void *value, pred_t eq_func) {
    int next;

    if (l_is_empty(list)) {
        return ITEM_NOT_FOUND;
    }

    if ((*eq_func)(value, list->value)) {
        return 0;
    }

    return (next = l_index_of(list->next, value, eq_func)) == ITEM_NOT_FOUND
           ? next
           : next + 1;
}

size_t l_size(list_t list) {
    return l_is_empty(list)
           ? 0
           : l_size(list->next) + 1;
}

void *l_get(list_t list, unsigned int index) {
    return !index
           ? list->value
           : l_get(list->next, index - 1);
}

void l_add_all(list_t *list, pred_t cmp_func, int argc, ...) {
    int     i;
    va_list args;
    va_start(args, argc);

    for (i = 0; i < argc; ++i) {
        if (!cmp_func) {
            l_append(list, va_arg(args, void *));
        } else {
            l_add_sorted(list, va_arg(args, void *), cmp_func);
        }
    }

    va_end(args);
}

void l_add_sorted(list_t *list, void *value, pred_t cmp_func) {
    return l_is_empty(*list) || (*cmp_func)(value, (*list)->value)
           ? l_prepend(list, value)
           : l_add_sorted(&(*list)->next, value, cmp_func);
}

void l_append(list_t *list, void *value) {
    return l_is_empty(*list)
           ? l_prepend(list, value)
           : l_append(&(*list)->next, value);
}

void l_prepend(list_t *list, void *value) {
    cell_t *new;

    if (!(new = (cell_t *) malloc(sizeof(cell_t)))) {
        perror("l_prepend");
        exit(errno);
    }

    new->value = value;
    new->next  = *list;
    *list = new;
}

void l_remove(list_t *list, void *value, pred_t eq_func, free_fn ffn) {
    return l_is_empty(*list) || (*eq_func)(value, (*list)->value)
           ? l_remove_first(list, ffn)
           : l_remove(&(*list)->next, value, eq_func, ffn);
}

void l_remove_at(list_t *list, unsigned int index, free_fn ffn) {
    if (index >= l_size(*list)) {
        fprintf(stderr, "l_remove_at: index out of bonds.\n");
        return;
    }

    return !index
           ? l_remove_first(list, ffn)
           : l_remove_at(&(*list)->next, index - 1, ffn);
}

void l_remove_first(list_t *list, free_fn ffn) {
    cell_t *current = *list;

    if (current) {
        *list = current->next;
        if ((*ffn)) {
            (*ffn)(current->value);
        }
        free(current);
    }
}

void l_remove_last(list_t *list, free_fn ffn) {
    return l_is_empty(*list) || !(*list)->next
           ? l_remove_first(list, ffn)
           : l_remove_last(&(*list)->next, ffn);
}

void l_clear(list_t *list, free_fn ffn) {
    while (!l_is_empty(*list)) {
        l_remove_first(list, ffn);
    }

    *list = NULL;
}
