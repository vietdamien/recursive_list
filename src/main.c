/* *****************************************************************************
 * Project name: List
 * File name   : main
 * Author      : Damien Nguyen
 * Date        : Friday, March 06 2020
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"

bool equal_str(const char *s1, const char *s2) {
    return !strcmp(s1, s2);
}

bool sorted_str(const char *s1, const char *s2) {
    return strcmp(s1, s2) < 0;
}

int main() {
    list_t list = NULL;

    return EXIT_SUCCESS;
}

void l_print(list_t list) {
    printf("List: ");

    while (list) {
        printf("%16s", (char *) list->value);
        list = list->next;
    }

    printf("\n");
}

