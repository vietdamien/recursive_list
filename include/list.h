/* *****************************************************************************
 * Project name: List
 * File name   : list
 * Author      : Damien Nguyen
 * Date        : Friday, March 06 2020
 * ****************************************************************************/

#ifndef _LIST_LIST_H_
#define _LIST_LIST_H_

#include <stdbool.h>

// ========================== CONSTANTS AND MACROS ========================== //
#define ITEM_NOT_FOUND -1

// =============================== STRUCTURES =============================== //
typedef struct cell {
    void *value;
    struct cell *next;
} cell_t, *list_t;

typedef bool (*pred_t)(const void *, const void *);
typedef void (*free_fn)(void *);

// ============================== DECLARATIONS ============================== //
bool l_is_empty(list_t list);

int l_index_of(list_t list, void *value, pred_t eq_func);

size_t l_size(list_t list);

void *l_get(list_t list, unsigned int index);

void l_add_all(list_t *list, pred_t cmp_func, int argc, ...);
void l_add_sorted(list_t *list, void *value, pred_t cmp_func);
void l_append(list_t *list, void *value);
void l_prepend(list_t *list, void *value);

void l_remove(list_t *list, void *value, pred_t eq_func, free_fn ffn);
void l_remove_at(list_t *list, unsigned int index, free_fn ffn);
void l_remove_first(list_t *list, free_fn ffn);
void l_remove_last(list_t *list, free_fn ffn);

void l_clear(list_t *list, free_fn ffn);

void l_print(list_t list);

#endif // _LIST_LIST_H_
